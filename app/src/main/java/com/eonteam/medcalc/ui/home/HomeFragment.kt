package com.eonteam.medcalc.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.ParseException
import androidx.fragment.app.Fragment
import com.eonteam.medcalc.databinding.FragmentHomeBinding
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.button.setOnClickListener {
            val dt: String = binding.dateField.text.toString().ifEmpty {
                SimpleDateFormat("dd/MM/yyyy").format(Date().time).toString()
            }
             // Start date

            val sdf = SimpleDateFormat("dd/MM/yyyy")
            val c: Calendar = Calendar.getInstance()
            try {
                c.time = sdf.parse(dt)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.add(
                Calendar.DATE,
                280
            ) // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE

            val sdf1 = SimpleDateFormat("dd/MM/yyyy")
            val output: String = sdf1.format(c.time)
            binding.textHome.text = String.format("Data aproximada: %s", output)
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}